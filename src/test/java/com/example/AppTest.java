package com.example;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.example.t51365.model.ApprovalGroup;
import com.example.t51365.model.ApprovalList;
import com.example.t51365.model.ApprovalRequest;
import com.example.t51365.model.RowData;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.drools.core.command.runtime.process.StartProcessCommand;
import org.drools.core.command.runtime.rule.InsertObjectCommand;
import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.runtime.ExecutionResults;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNMessage;
import org.kie.dmn.api.core.DMNResult;
import org.kie.server.api.marshalling.Marshaller;
import org.kie.server.api.marshalling.MarshallerFactory;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.api.model.KieServiceResponse.ResponseType;
import org.kie.server.client.DMNServicesClient;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    private String fancyPrefix = "[" + this.getClass().getPackage().getName() + "] ..::|| ";

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    public void callDMN(KieServicesClient client, String dmn_container, String dmn_modelNameSpace, String dmn_modelName,
            HashMap<String, Object> dmnData) {

        DMNServicesClient dmnClient = client.getServicesClient(DMNServicesClient.class);
        DMNContext dmnContext = dmnClient.newContext();

        dmnData.keySet().forEach(key -> {
            dmnContext.set(key, dmnData.get(key));
        });

        ServiceResponse<DMNResult> serverResp = dmnClient.evaluateAll(dmn_container, dmn_modelNameSpace, dmn_modelName,
                dmnContext);
        DMNResult dmnResult = serverResp.getResult();

        for (DMNDecisionResult dr : dmnResult.getDecisionResults()) {
            System.out.println(fancyPrefix + "Decision: '" + dr.getDecisionName() + "', " + dr.getEvaluationStatus()
                    + ", " + "Result: " + dr.getResult());
            for (DMNMessage msg : dr.getMessages()) {
                System.out.println(fancyPrefix + "Decision Messages: '" + msg);
            }
        }

    }

    public ApprovalRequest createDataForAgFlow() {
        // public ApprovalRequest createDataForAgFlow(Map<String, Object> bpmnDataMap) {
        ObjectMapper omap = new ObjectMapper();

        ArrayList<RowData> rowList = new ArrayList<>();
        {
            ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA1");
            ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB1");

            ApprovalList approvalList = new ApprovalList();
            approvalList.addApprovalGroup(ag1);
            approvalList.addApprovalGroup(ag2);

            RowData rd = new RowData(1, 0, 10, null, null);
            rd.addApprovalList(approvalList);

            rowList.add(rd);
        }
        {
            ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA2");
            ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB2");

            ApprovalList approvalList = new ApprovalList();
            approvalList.addApprovalGroup(ag1);
            approvalList.addApprovalGroup(ag2);

            RowData rd = new RowData(2, 10, 50, null, null);
            rd.addApprovalList(approvalList);

            rowList.add(rd);
        }
        {
            RowData rd = new RowData(3, 50, 100, null, null);
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA3");
                ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB3");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                approvalList.addApprovalGroup(ag2);
                rd.addApprovalList(approvalList);
            }
            rowList.add(rd);
        }
        {
            RowData rd = new RowData(4, null, null, "retail", null);
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA3");
                ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB3");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                approvalList.addApprovalGroup(ag2);
                rd.addApprovalList(approvalList);
            }
            rowList.add(rd);
        }
        {
            RowData rd = new RowData(5, null, null, "business", null);
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA3");
                ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB3");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                approvalList.addApprovalGroup(ag2);
                rd.addApprovalList(approvalList);
            }
            rowList.add(rd);
        }
        {
            RowData rd = new RowData(4, 50, 100, null, null);
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupC3");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                rd.addApprovalList(approvalList);
            }

            rowList.add(rd);
        }
        ApprovalRequest ar = new ApprovalRequest();

        ar.setRequestNumber(75);
        ar.setRowData(rowList);

        return ar;
    }

    /**
     * Rigorous Test :-)
     */
    public void testApp() {

        boolean invokeDMN = true;

        String url = "http://localhost:8080/kie-server/services/rest/server";
        String username = "kieServerUser";
        String password = "kieServerUser1234;";

        KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(url, username, password);

        config.setMarshallingFormat(MarshallingFormat.JSON);

        Set<Class<?>> allClasses = new HashSet<Class<?>>();
        allClasses.add(ApprovalGroup.class);
        allClasses.add(ApprovalList.class);
        allClasses.add(RowData.class);
        allClasses.add(ApprovalRequest.class);
        config.addExtraClasses(allClasses);

        KieServicesClient client = KieServicesFactory.newKieServicesClient(config);
        RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
        KieCommands kieCommander = KieServices.Factory.get().getCommands();

        //
        // -- invoke "ag_flow2" BPMN model
        //
        {
            String container = "tmns";
            ObjectMapper omap = new ObjectMapper();

            Map<String, Object> ruleFlowMap = new HashMap<>();

            ApprovalRequest ar = createDataForAgFlow();

            List<Command<?>> commands = new ArrayList<Command<?>>();
            commands.add(new InsertObjectCommand(ar, "approval-request"));
            commands.add(new StartProcessCommand("morf2.ag_flow2", ruleFlowMap));
            BatchExecutionCommand batchCommand = kieCommander.newBatchExecution(commands, "myStatelessSession");
            try {
                Marshaller marshallerJSON = MarshallerFactory.getMarshaller(allClasses, MarshallingFormat.JSON,
                        getClass().getClassLoader());
                String bbJSON = marshallerJSON.marshall(batchCommand);
                FileWriter fw = new FileWriter(new File(("ag_flow2.json")));
                fw.write(bbJSON);
                fw.close();
                System.out.println(fancyPrefix + "batchCommand has been saved as JSON to ag_flow2.json");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ServiceResponse<ExecutionResults> response = ruleClient.executeCommandsWithResults(container, batchCommand);

            if (response.getType().compareTo(ResponseType.SUCCESS) == 0) {
                System.out.println(fancyPrefix);
                System.out.println(fancyPrefix + "Response: " + response.getMsg() + " ||::..");
                System.out.println(fancyPrefix);
            }

            ApprovalRequest approvalDecisionResult = (ApprovalRequest) response.getResult()
                    .getValue("approval-request");
            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix + "APPROVAL DECISION :" + approvalDecisionResult.getDecision() + " ||::..");
            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix);
        }

        //
        // --- Invoke DMN ---
        //
        if (invokeDMN) {
            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix + "INVOKING DMN");
            System.out.println(fancyPrefix);

            String dmn_container = "tmns";
            String dmn_modelName = "ar_ag2";
            String dmn_modelNameSpace = "https://kiegroup.org/dmn/_E19245A7-5775-469D-9A0F-8F81210F1F43";

            ObjectMapper omap = new ObjectMapper();

            HashMap<String, Object> dmnData = new HashMap<>();

            ArrayList<RowData> rowList = new ArrayList<>();
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA1");
                ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB1");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                approvalList.addApprovalGroup(ag2);

                RowData rd = new RowData(1, 0, 10, "retail", null);
                rd.addApprovalList(approvalList);

                rowList.add(rd);
            }
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA2");
                ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB2");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                approvalList.addApprovalGroup(ag2);

                RowData rd = new RowData(2, 10, 50, null, null);
                rd.addApprovalList(approvalList);

                rowList.add(rd);
            }
            {
                RowData rd = new RowData(3, 50, 100, null, null);
                {
                    ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA3");
                    ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB3");

                    ApprovalList approvalList = new ApprovalList();
                    approvalList.addApprovalGroup(ag1);
                    approvalList.addApprovalGroup(ag2);
                    rd.addApprovalList(approvalList);
                }
                {
                    ApprovalGroup ag1 = new ApprovalGroup(1, "GroupC3");

                    ApprovalList approvalList = new ApprovalList();
                    approvalList.addApprovalGroup(ag1);
                    rd.addApprovalList(approvalList);
                }

                rowList.add(rd);
            }
            {
                RowData rd = new RowData(4, 0, 0, null, "CREATE_ROLE");
                {
                    ApprovalGroup ag1 = new ApprovalGroup(2, "DEFAULT_APPROVERS_GROUP");

                    ApprovalList approvalList = new ApprovalList();
                    approvalList.addApprovalGroup(ag1);
                    rd.addApprovalList(approvalList);
                }

                rowList.add(rd);
            }
            ApprovalRequest ar = new ApprovalRequest();
            ar.setRequestNumber(6);
            ar.setRequestService("retail");
            // ar.setRequestAction("CREATE_ROLE");
            ar.setRowData(rowList);

            dmnData.put("ApprovalRequest", omap.convertValue(ar, ApprovalRequest.class));

            callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
        }

        assertTrue(true);
    }

}
